//package calculator;

//import static org.hamcrest.CoreMatchers.equalTo;
//import static org.hamcrest.CoreMatchers.is;
//import static org.junit.Assert.assertThat;
//
//import calculator.UpnCalculator;
//import io.cucumber.java.en.Given;
//import io.cucumber.java.en.Then;
//import io.cucumber.java.en.When;

//import org.junit.*;
//import org.junit.runner.RunWith;
//import static org.junit.Assert.assertEquals;
//
//public class CalculatorTest {
// 
//	@Test
//	public void testSimpleAdd() {
////		calcuator = new Calculator();
//		int result = Calculator.Add("2,3");
//		Assert.assertEquals("not correct", 5, result);
//		/*
//		 * "加法有问题"：期望值和实际值不一致时，显示的信息
//		 * 5 ：期望值
//		 * result ：实际值
//		 */
//	}
//}

package calculator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import calculator.UpnCalculator;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CalculatorTest {
	
	Calculator calc = new Calculator();

	@Given("the first input is {int}")
	public void theFirstInputIs(String text) {
	    calc.add(text);
	}

//	@Given("the second input is {int}")
//	public void theSecondInputIs(Integer int1) {
//	    calc.input(int1);
//	}
//
//	@When("the add button is pressed")
//	public void theAddButtonIsPressed() {
//	    calc.add();
//	}

	@Then("{int} is shown on the display.")
	public void theIsShown(Integer int1) {
	    assertThat(calc.display(),is(equalTo(int1)));
	}
}


